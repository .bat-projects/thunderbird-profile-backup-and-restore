Thunderbird profile backup and restore batch

Can be run as is
    Have two run options when running
        B auto backup
        R direct to restore menu
        
This batch backups the thunderbird profile to documents\Thunderbird_Backup
It keeps upto 3 different backups to allow for more recover options but not so much to use too much space

When running, It will first attempt to close thunderbird down by normal means. If unable, it will force close it
