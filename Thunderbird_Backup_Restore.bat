:: Thunderbird backup and restore batch file
:: Created by SimpleWalker
:: Version 1.0.1
:: ------------------------
:: Batch basic info
@ECHO OFF
TITLE Thunderbird Backup / Restore
ECHO Thunderbird Backup / Restore
Echo Version 1.0.1
ECHO ______________________________
ECHO:
:: ------------------------
:: Setup variable for choice
SET option=%1
:: ------------------------
:: Check variable input
:input
IF NOT DEFINED option GOTO menu
IF /I %option%==B GOTO backup
IF %option%==1 GOTO backup
IF /I %option%==R GOTO restore
IF %option%==2 GOTO restore
IF %option%==3 GOTO exit
:: ------------------------
:: Menu
:menu
ECHO Menu:
ECHO -----
ECHO:
ECHO 1. Backup
ECHO 2. Restore
ECHO 3. Exit
SET /p option=Enter choice: 
CLS
GOTO input
:: ------------------------
:: Backup
:backup
:: ------------------------
:: Check if thunderbird is running and kill it if running
TASKLIST | FIND /I /N "thunderbird.exe">NUL
IF "%ERRORLEVEL%"=="0" (
	ECHO Attempting to close thunderbird
	TASKKILL /IM thunderbird.exe
	PING localhost -n 3 > nul
)
TASKLIST | FIND /I /N "thunderbird.exe">NUL
IF "%ERRORLEVEL%"=="0" (
	ECHO Forcing close of thunderbird
	TASKKILL /F /IM thunderbird.exe
)
:: ------------------------
:: Create mirror backup with currrent date
ROBOCOPY %AppData%\Thunderbird %UserProfile%\Documents\Thunderbird_Backup\TB_%date:~-4,4%"-"%date:~-7,2%"-"%date:~-10,2% /mir /v
:pastBackups
:: ------------------------
:: Count current number of backups
FOR /f %%f IN ('DIR %UserProfile%\Documents\Thunderbird_Backup\ /B ^| FIND /V /C ""') DO SET /A backupCount=%%f
:: ------------------------
:: Move Backup list out of IF as it does not work for some reason
DIR %UserProfile%\Documents\Thunderbird_Backup\ /B > tmp
SET /p oldestBackup=< tmp
DEL tmp
IF %backupCount% LEQ 3 GOTO exit
:: ------------------------
:: Delete oldest backup if there is more than 3
IF %backupCount% GTR 3 (
	:: IF check if oldestBackup exist, if not. error out ------------------------------------------------------------------------
	RD /S /Q %UserProfile%\Documents\Thunderbird_Backup\%oldestBackup%
)
GOTO pastBackups
:: ------------------------
:: Restore
:restore
:: ------------------------
:: Count current number of backups
FOR /f %%f IN ('DIR %UserProfile%\Documents\Thunderbird_Backup\ /B ^| FIND /V /C ""') DO SET /A backupCount=%%f
IF %backupCount% EQU 0 ECHO No Backups Available && GOTO menu
:: ------------------------
:: List all available backups and ask for selection
ECHO List of available backups: (Oldest ordered first)
ECHO:
SETLOCAL enableextensions enabledelayedexpansion
SET /A index=0
DIR %UserProfile%\Documents\Thunderbird_Backup\ /B > tmp
for /f "delims=" %%a in (tmp) DO (
	SET /A index+=1
	ECHO !index!: %%a
)
SET /A index+=1
ECHO !index!: Back
ENDLOCAL
SET /A backupCount = %backupCount% + 1
SET option=
SET /p option=Enter choice: 
IF %option% LSS 1 CLS && ECHO Invaild Input && GOTO restore
IF %option% EQU %backupCount% CLS && GOTO menu
IF %option% GTR %backupCount% CLS && ECHO Invaild Input && GOTO restore

SET selectedBackup=
set /A skipTo=%option%-1
IF %option% EQU 1 (
	SET /p selectedBackup=< tmp
) ELSE (
	FOR /f "skip=%skipTo%" %%a IN (tmp) DO SET "selectedBackup=%%a" & GoTo:next
)
:next
DEL tmp
ECHO:
ECHO Use %selectedBackup% to recover?
SET /p confirm=Confirm (y/n):
IF /I NOT %confirm%==Y CLS && GOTO restore
:: ------------------------
:: Check if thunderbird is running and kill it if running
TASKLIST | FIND /I /N "thunderbird.exe">NUL
IF "%ERRORLEVEL%"=="0" (
	ECHO Attempting to close thunderbird
	TASKKILL /IM thunderbird.exe
	PING localhost -n 3 > nul
)
TASKLIST | FIND /I /N "thunderbird.exe">NUL
IF "%ERRORLEVEL%"=="0" (
	ECHO Forcing close of thunderbird
	TASKKILL /F /IM thunderbird.exe
)
:: ------------------------
:: Recover selected backup
ROBOCOPY %UserProfile%\Documents\Thunderbird_Backup\%selectedBackup% %AppData%\Thunderbird /mir /v

:exit
ECHO Done
